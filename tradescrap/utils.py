import csv
import os

global_offers_table_header = [
    'num',
    'account_number',
    'account_type',
    'trader_name',
    'trader_country',
    'trader_strategy',
    'trader_id',
    'profit_usd',
    'profit_pips',
    'profit_%',
    'max_draw_down',
    'training_days',
    'balance',
    'trades_closed',
    'trades_opened',
    'investors_quantity',
    'investors_portfolio'
]

default_path = './data/'


def _get_trader_path(trader_id, path):
    if not os.path.exists(path):
        os.mkdir(path)
    trading_path = os.path.join(path, 'trading')
    if not os.path.exists(trading_path):
        os.mkdir(trading_path)
    trader_path = os.path.join(trading_path, str(trader_id))
    if not os.path.exists(trader_path):
        os.mkdir(trader_path)
    return trader_path


def save_trader_profit(trader_id, data, path):
    headers = ['timestamp', 'profitability']
    trader_path = _get_trader_path(trader_id, path)

    with open(os.path.join(trader_path, 'profit.csv'), 'w+') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=headers,
                                quoting=csv.QUOTE_ALL)
        writer.writeheader()
        for row in data:
            writer.writerow(row)


def save_trader_balance(trader_id, data, path):
    headers = [
        'timestamp',
        'balance_min',
        'balance_max',
        'equity_min',
        'equity_max'
    ]
    trader_path = _get_trader_path(trader_id, path)
    with open(os.path.join(trader_path, 'balance.csv'), 'w+') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=headers,
                                quoting=csv.QUOTE_ALL)
        writer.writeheader()
        for row in data:
            writer.writerow(row)


def save_traders_offers(data, path):
    """Save trader's offers extended information taken from profiles"""
    header = sorted(list(data[0].keys()))
    if not os.path.exists(path):
        os.mkdir(path)
    with open(os.path.join(path, 'offers.csv'), 'w+') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=header,
                                quoting=csv.QUOTE_ALL)
        writer.writeheader()
        for row in data:
            writer.writerow(row)


def load_trading_history(trader_id, path):
    # order is important
    headers = [
        'trade_number',
        'close_time',
        'type',
        'volume',
        'symbol',
        'open_price',
        'close_price',
        'profit_result'
    ]
    trader_path = _get_trader_path(trader_id, path)
    csv_file = os.path.join(trader_path, 'history.csv')
    with open(csv_file, 'r+') as csv_file:
        reader = csv.DictReader(csv_file,
                                fieldnames=headers,
                                quoting=csv.QUOTE_ALL)
        try:
            # skip header
            next(reader)
            result = [row for row in reader]
            return result
        except StopIteration as e:
            return []


def save_trading_history(trader_id, data, path):
    """Save a trading history taken from :trader_id profile"""

    # order is important
    headers = [
        'trade_number',
        'close_time',
        'type',
        'volume',
        'symbol',
        'open_price',
        'close_price',
        'profit_result'
    ]

    trader_path = _get_trader_path(trader_id, path)
    csv_file = os.path.join(trader_path, 'history.csv')

    if os.path.exists(csv_file):
        rows = load_trading_history(trader_id, path)
        stored = {r['trade_number']: r for r in rows}
        update = {r['trade_number']: r for r in data}
        stored.update(update)
        data = stored.values()

    with open(csv_file, 'w+') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=headers,
                                quoting=csv.QUOTE_ALL)
        writer.writeheader()
        for row in data:
            writer.writerow(row)


def save_global_offers_table(data, path):
    """Save items parsed from table pages to csv file"""
    path = path or os.path.join(default_path, 'table.csv')
    if not os.path.exists(os.path.dirname(path)):
        os.mkdir(os.path.dirname(path))

    with open(path, 'w+') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=global_offers_table_header,
                                quoting=csv.QUOTE_ALL)
        writer.writeheader()
        for row in data:
            writer.writerow(row)


def load_global_offers_table(path=None):
    """Load items parsed from pages to csv file"""

    path = path or os.path.join(default_path, 'table.csv')
    with open(path, 'r+') as csv_file:
        reader = csv.DictReader(csv_file,
                                fieldnames=global_offers_table_header,
                                quoting=csv.QUOTE_ALL)
        try:
            # skip header
            next(reader)
            result = [row for row in reader]
            return result
        except StopIteration as e:
            return []


def load_traders_id_list(path=None):
    path = path or os.path.join(default_path, 'traders.csv')
    with open(path, 'r+') as csv_file:
        traders = [int(x) for x in csv_file.readlines()]
    return traders
