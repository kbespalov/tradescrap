import concurrent
import requests
import logging
import argparse

import sys
from tenacity import retry, TryAgain
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
from tenacity import stop_after_attempt

from tradescrap.utils import load_global_offers_table
from tradescrap.utils import save_trading_history, save_traders_offers, \
    load_traders_id_list, save_trader_balance, save_trader_profit

logging.basicConfig(format='%(levelname)s:%(message)s')
LOG = logging.getLogger(__name__)


class CopyFxEndpoint(object):
    def __init__(self, endpoint, base=None):
        self.session = requests.Session()
        self.session.headers.update({'X-Requested-With': 'XMLHttpRequest'})
        self.base = base or 'http://www.copyfx.com/pamm/ajax/'
        self.endpoint = self.base + endpoint

    @retry(stop=stop_after_attempt(3))
    def get(self, params):
        response = self.session.get(self.endpoint, params=params)
        if response.status_code != 200:
            raise TryAgain("Fail to access to an endpoint %s", self.endpoint)
        return response


class TradingHistory(CopyFxEndpoint):
    """A class that represents a trading history endpoint"""

    def __init__(self, config):
        super(TradingHistory, self).__init__('memberTradingHistory')
        self.config = config
        # order is important
        self.data_table_headers = [
            'trade_number',
            'close_time',
            'type',
            'volume',
            'symbol',
            'open_price',
            'close_price',
            'profit_result'
        ]

    def get_by(self, member_id, order='DESC'):
        """Dumps all pages of a trader's history"""
        result = []
        LOG.debug('[%s] Dumping trading history info', member_id)
        params = {
            'member_id': member_id,
            'member_type': 'provider',
            'order_way': order,
            'order_by': 'ticket'
        }
        page, pages = 1, 1
        while page <= pages:
            if int(self.config.limit) and page > int(self.config.limit):
                break
            params.update({'page': page})
            response = self.get(params=params)
            content = response.content
            html = BeautifulSoup(content, 'lxml')
            # extract last <a> link on pagination
            pages = int(
                html.findAll('a', attrs={'class': 'set-page'})[-1].text)
            if pages != 1:
                LOG.debug('[%s] History pages: %s', member_id, pages)
            # process the html table
            table = html.find('tbody')
            for row in table.findAll('tr'):
                try:
                    data = row.findAll('td')
                    mapped = dict(zip(self.data_table_headers, data))
                    mapped = self.process_table_row(mapped)
                    result.append(mapped)
                except Exception as e:
                    LOG.exception('Failed to parse history row %s', params)

            LOG.debug('[%s] The %s history page was parsed', member_id, page)
            page += 1
        return result

    @staticmethod
    def process_table_row(row):
        """Convert a vales of row items from html to python objects"""
        row['close_price'] = float(row['close_price'].text.strip() or 0)
        row['open_price'] = float(row['open_price'].text.strip() or 0)
        row['profit_result'] = float(row['profit_result'].text.strip() or 0)
        row['trade_number'] = int(row['trade_number'].text.strip())
        row['volume'] = float(row['volume'].text.strip() or 0)
        row['type'] = row['type'].text.strip()
        row['symbol'] = row['symbol'].text.strip()
        row['close_time'] = row['close_time'].text.strip()
        return row


class OfferInformation(CopyFxEndpoint):
    """An information taken trader's profile"""

    def __init__(self, config):
        super(OfferInformation, self).__init__('fetchCurrentOfferInfo')
        self.config = config

    def get_by(self, member_id):
        params = {'member_type': 'provider', 'member_id': member_id}
        response = self.get(params)
        try:
            mapped = response.json()
            mapped['trader_id'] = member_id
            if not mapped['currency']:
                mapped['currency'] = ''
            # the currency parameter can consist of html body
            if 'class' in mapped['currency']:
                mapped['currency'] = mapped['currency'].split('"')[1]
            return mapped
        except ValueError:
            LOG.exception('Failed to get offer information for %s', member_id)


class TradingProfitability(CopyFxEndpoint):
    """An information taken trader's profile"""

    def __init__(self, config):
        super(TradingProfitability, self).__init__('fetchProfitInfo')
        self.config = config

    def get_by(self, member_id, period='all'):
        """Get time series info of trader's profitability from API"""
        LOG.debug('[%s] Dumping account profit time-series data', member_id)
        params = {
            'member_id': member_id,
            'period': period,
            'member_type': 'provider'
        }
        response = self.get(params)
        return self._as_csv(response)

    def _as_csv(self, response):
        """Transform api's json formatted response to the csv table"""
        header = ['timestamp', 'profitability']
        data = response.json()
        days = data['chart_data']['day_stats']
        result = [dict(zip(header, day)) for day in days]
        return result


class AccountBalance(CopyFxEndpoint):
    """An information taken trader's profile"""

    def __init__(self, config):
        base = 'http://www.copyfx.com/cfxrating/charts/'
        endpoint = 'getBalanceAndEquityChartData'
        super(AccountBalance, self).__init__(endpoint, base)
        self.config = config

    def get_by(self, member_id, period='all'):
        LOG.debug('[%s] Dumping account balance time-series data', member_id)
        params = {
            'member_id': member_id,
            'period': period,
            'member_type': 'provider'
        }

        response = self.get(params)
        return self._as_csv(response)

    def _as_csv(self, response):
        result = []
        header = [
            'timestamp',
            'balance_min',
            'balance_max',
            'equity_min',
            'equity_max'
        ]
        data = response.json()['data']

        # list of pairs (timestamp, value)
        balance_min = data['balance_min']
        balance_max = data['balance_max']
        equity_min = data['equity_min']
        equity_max = data['equity_max']

        for p in zip(balance_min, balance_max, equity_max, equity_min):
            linearized = [p[1][0]]
            linearized.extend([v[1] for v in p])
            result.append(dict(zip(header, linearized)))
        return result


def dump_profile(trader_id, config):
    """Dumps a trader profile"""
    offer, history, profit, balance = None, None, None, None
    try:
        if 'offer' in config.required_data:
            offer = OfferInformation(config).get_by(trader_id)
        if 'history' in config.required_data:
            history = TradingHistory(config).get_by(trader_id)
        if 'profit' in config.required_data:
            profit = TradingProfitability(config).get_by(trader_id)
        if 'balance' in config.required_data:
            balance = AccountBalance(config).get_by(trader_id)
    except Exception:
        LOG.exception('[%s] Failed to dump profile', trader_id)

    return trader_id, offer, history, profit, balance


def get_traders_ids(config):
    """Load list of traders ids from dumped csv or just list of ids per line"""
    if config.type == 'dump':
        # load from dumped csv file
        traders = load_global_offers_table(config.input_file)
        traders = [r['trader_id'] for r in traders]
    else:
        # load from file with list of ids
        traders = load_traders_id_list(config.input_file)
    return traders


def dump_profiles(config):
    futures, results = [], []
    offers = []
    with concurrent.futures.ThreadPoolExecutor(
            max_workers=config.threads
    ) as executor:
        for trader_id in get_traders_ids(config):
            if not trader_id:
                LOG.error('The input file has wrong type. See --help')
                exit(1)
            futures.append(executor.submit(dump_profile, trader_id, config))

        for future in concurrent.futures.as_completed(futures):
            trader_id, offer, history, profit, balance = future.result()
            if offer:
                offers.append(offer)
            if history:
                save_trading_history(trader_id, history, config.output_dir)
            if profit:
                save_trader_profit(trader_id, profit, config.output_dir)
            if balance:
                save_trader_balance(trader_id, balance, config.output_dir)

        save_traders_offers(offers, config.output_dir)

    return results


def parse_args(args):
    description = 'CLI tool for dumping traders profiles'
    formatter = argparse.ArgumentDefaultsHelpFormatter
    parser = argparse.ArgumentParser(description=description,
                                     formatter_class=formatter)

    parser.add_argument('-o', '--out',
                        default='./data',
                        dest='output_dir',
                        help='an output directory')
    parser.add_argument('-i', '--in',
                        default='./data/table.csv',
                        dest='input_file',
                        help='a file that contains a list of traders id in the'
                             'specified format (see --type).')
    parser.add_argument('-t', '--type',
                        choices=['dump', 'list'],
                        default='dump',
                        dest='type',
                        help='a type of an input file.')
    parser.add_argument('--threads', dest='threads', default=4)
    parser.add_argument('-g', '--get', action='append',
                        dest='required_data',
                        default=['offer', 'history', 'profit', 'balance'],
                        help='list of required data types')
    parser.add_argument('-l', '--limit', dest='limit', default=0,
                        help='the limit of the history pages')
    parser.add_argument('--debug', action='store_true', dest='debug')
    return parser.parse_args(args)


def main():
    config = parse_args(sys.argv[1:])
    if config.debug:
        LOG.setLevel(logging.DEBUG)

    dump_profiles(config)


if __name__ == '__main__':
    main()
