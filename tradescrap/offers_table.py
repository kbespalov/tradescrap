import argparse
import sys
from bs4 import BeautifulSoup
from tradescrap.utils import save_global_offers_table
import requests
import logging

LOG = logging.getLogger(__name__)

base_url = 'http://www.copyfx.com'
table_url = base_url + '/ratings/traders-with-offers' \
                       '/offers-all/all/?pg=%s&on_page=%s'

# represent list of columns of the trade table (order is important)

columns_map = ['num',
               'trader',
               'profit_usd',
               'profit_pips',
               'profit_%',
               'max_draw_down',
               'training_days',
               'balance',
               'trades_closed',
               'trades_opened',
               'investors_quantity',
               'investors_portfolio']


def process_row(row):
    """Parse html based tags and extract required information"""

    # unwrap trader information from raw html

    trader = row['trader']
    del row['trader']

    account_info = trader.findAll('p')[1].text
    account_info = account_info.strip()

    # #2842405 Pro-Standard
    row['account_number'] = account_info.split(' ')[0]
    row['account_type'] = ' '.join(account_info.split(' ')[1:])

    row['trader_name'] = trader.find(attrs={'class': 'trader__name'}).text
    row['trader_strategy'] = trader.find(attrs={'class': 'strategy__name'})

    # a strategy may not present
    if row['trader_strategy']:
        row['trader_strategy'] = row['trader_strategy'].text
    else:
        row['trader_strategy'] = 'N/A'

    # parse the link to a trader profile
    link = trader.find('a').attrs['href']
    trader_id = link.split('/ratings/rating-all/show/')[1]
    trader_id = trader_id.split('/period/all/')[0]
    row['trader_id'] = trader_id

    row['trader_country'] = trader.find('img').attrs['title']

    # list of numeric columns names
    numeric_columns = set(list(row.keys())) - {'trader_name',
                                               'account_type',
                                               'account_number',
                                               'trader_country',
                                               'trader_strategy',
                                               'trader_id'}
    # convert to python int or float
    for name in numeric_columns:
        text = row[name].text
        num = [c for c in text if c in ['+', '-', ',', '.'] or c.isdigit()]
        num = ''.join(num)

        if name in ['num', 'investors_quantity', 'training_days', 'trader_id']:
            row[name] = int(''.join([c for c in num if c.isdigit()]))
        else:
            row[name] = float(num)

    return row


def load_pages():
    """Iterate throw the pages of the site and parse the table"""

    page_number = 1
    items_per_page = 100
    result = []
    item_ids = set()

    while True:
        url = table_url % (page_number, items_per_page)
        response = requests.get(url)
        if response.status_code == 200:
            page_number += 1
            content = response.content
            html = BeautifulSoup(content, "lxml")
            main_table = html.find('table', {'class': ['table-rates']})
            rows = main_table.find_all('tr')
            for row in rows:
                columns_content = list(row.findAll('td'))
                if columns_content:
                    mapped = dict(zip(columns_map, columns_content))
                    mapped = process_row(mapped)
                    if mapped['num'] in item_ids:
                        return result
                    else:
                        item_ids.add(mapped['num'])
                        result.append(mapped)
        else:
            LOG.error('Failed to load %d page. Retrying...', url)


def parse_args(args):
    description = 'CLI tool for dumping a main table of the offers'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-o', '--out',
                        default='./data/traders.csv',
                        dest='output_file',
                        help='an output file')

    return parser.parse_args(args)


def main():
    config = parse_args(sys.argv[1:])
    data = load_pages()
    save_global_offers_table(data, config.output_file)


if __name__ == '__main__':
    main()
