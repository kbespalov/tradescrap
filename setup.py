from setuptools import setup

requirements = [
    'requests',
    'bs4',
    'lxml',
    'futures',
    'tenacity'
]

setup(name='copyfix scrapper',
      version='0.1.0',
      packages=['tradescrap'],
      install_requires=requirements,
      entry_points={
          'console_scripts': [
              'dump_table=tradescrap.offers_table:main',
              'dump_profiles=tradescrap.profiles:main'
          ]
      })
